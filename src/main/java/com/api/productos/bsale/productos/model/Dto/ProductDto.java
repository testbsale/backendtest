package com.api.productos.bsale.productos.model.Dto;

import lombok.*;

@Setter
@Getter
@Builder
@ToString
public class ProductDto {
    private int id;
    private String name;
    private String url_image;
    private float price;
    private int discount;
    private int category;
}
