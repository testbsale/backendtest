package com.api.productos.bsale.productos.model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;



@Builder
@AllArgsConstructor
@Data
@ApiModel(description = "products for the store")
public class Category {
    @ApiModelProperty(notes = "The id category : could be 1 or 10 ")
    private int id;
    @ApiModelProperty(notes = "The category name : could be bebida energetica or ron")
    private String name;
}
