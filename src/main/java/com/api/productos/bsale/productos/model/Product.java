package com.api.productos.bsale.productos.model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;


@Builder
@AllArgsConstructor
@Data
@ApiModel(description = "products for the store")
public class Product {
    @ApiModelProperty(notes = "The id product: could be 1 or 10 ")
    private int id;
    @ApiModelProperty(notes = "The product name : could be COCA COLA ZERO DESECHABLE or Papas Fritas Tarro")
    private String name;
    @ApiModelProperty(notes = "The Product image : url")
    private String url_image;
    @ApiModelProperty(notes = "Price of the product : could be 1500 or 2990")
    private float price;
    @ApiModelProperty(notes = "Discount of the product: could be 0 or 20")
    private int discount;
    @ApiModelProperty(notes = "Product category : could be 1 or 5")
    private int category;
}
