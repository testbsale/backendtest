package com.api.productos.bsale.productos.model.Dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;

@Setter
@Getter
@Builder
@ToString
public class PetitionDto {
    @ApiModelProperty(notes = "The id product: could be 1 or 10 ")
    private int idProduct;
    @ApiModelProperty(notes = "The product name : could be COCA COLA ZERO DESECHABLE or Papas Fritas Tarro")
    private String nameProduct;
    @ApiModelProperty(notes = "initial Price  of the product : could be 1500 or 2990")
    private float priceInitial;
    @ApiModelProperty(notes = "Final Price of the product : could be 1500 or 2990")
    private float priceFinal;
    @ApiModelProperty(notes = "Discount of the product: could be 0 or 20")
    private String discount;
    @ApiModelProperty(notes = "The id category : could be 1 or 10 ")
    private int idCategory;
    @ApiModelProperty(notes = "The category name : could be bebida energetica or ron")
    private String nameCategory;
    @ApiModelProperty(notes = "Page: its value must be numeric like 1 or 6 ")
    private int page;
    @ApiModelProperty(notes = "NumRow: number of records returned by the service, such as 10 or 25 records")
    private int numRow;


    public int offset() {
        return getPage() * getNumRow();
    }
    public void validatePetition(){
        if(getNumRow() == 0){
            setNumRow(5);
        }

    }
}


