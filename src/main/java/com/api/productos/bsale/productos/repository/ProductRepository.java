package com.api.productos.bsale.productos.repository;

import com.api.productos.bsale.productos.model.Category;
import com.api.productos.bsale.productos.model.Dto.PetitionDto;
import com.api.productos.bsale.productos.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ProductRepository {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public ProductRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<Category> consultCategories() {
        return jdbcTemplate.query("select * from category", (rs, arg1) -> new Category(rs.getInt("id"), rs.getString("name")));
    }

    public Integer ConsultProductsCount(PetitionDto petitionDto) {
        List<Integer> total =
                jdbcTemplate.query(
                        "SELECT count(p.id) as total " +
                                " FROM product p " +
                                " INNER JOIN category c on p.category = c.id " +
                                " WHERE CASE WHEN ? <> 0 THEN p.id = ? ELSE true END " +
                                " AND CASE WHEN ? <> '' THEN p.name like Concat('%',?,'%')  ELSE true END " +
                                " AND CASE WHEN ? <> ''  THEN p.discount <> 0 ELSE true END " +
                                " AND CASE WHEN ? <> 0.0 THEN p.price >= ? ELSE TRUE END " +
                                " AND CASE WHEN ? <> 0.0 THEN p.price <= ? ELSE TRUE END" +
                                " AND CASE WHEN ? <> 0 THEN c.id = ? ELSE true END " +
                                " AND CASE WHEN ? <> '' THEN c.name like Concat('%',?,'%') ELSE true END  ",
                        new Object[]{
                                petitionDto.getIdProduct(),
                                petitionDto.getIdProduct(),
                                petitionDto.getNameProduct(),
                                petitionDto.getNameProduct(),
                                petitionDto.getDiscount(),
                                petitionDto.getPriceInitial(),
                                petitionDto.getPriceInitial(),
                                petitionDto.getPriceFinal(),
                                petitionDto.getPriceFinal(),
                                petitionDto.getIdCategory(),
                                petitionDto.getIdCategory(),
                                petitionDto.getNameCategory(),
                                petitionDto.getNameCategory()
                        },
                        (rs, arg1) -> rs.getInt("total")
                );
        return total.get(0);
    }

    public List<Product> ConsultProducts(PetitionDto petitionDto) {
        String rowSql = "SELECT p.* " +
                " FROM product p " +
                " INNER JOIN category c on p.category = c.id " +
                " WHERE CASE WHEN ? <> 0 THEN p.id = ? ELSE true END " +
                " AND CASE WHEN ? <> '' THEN p.name like Concat('%',?,'%')  ELSE true END " +
                " AND CASE WHEN ? <> ''  THEN p.discount <> 0 ELSE true END " +
                " AND CASE WHEN ? <> 0.0 THEN p.price >= ? ELSE TRUE END " +
                " AND CASE WHEN ? <> 0.0 THEN p.price <= ? ELSE TRUE END" +
                " AND CASE WHEN ? <> 0 THEN c.id = ? ELSE true END " +
                " AND CASE WHEN ? <> '' THEN c.name like Concat('%',?,'%') ELSE true END" +
                " LIMIT ? OFFSET ? ";
        return jdbcTemplate.query(rowSql, new Object[]{
                petitionDto.getIdProduct(),
                petitionDto.getIdProduct(),
                petitionDto.getNameProduct(),
                petitionDto.getNameProduct(),
                petitionDto.getDiscount(),
                petitionDto.getPriceInitial(),
                petitionDto.getPriceInitial(),
                petitionDto.getPriceFinal(),
                petitionDto.getPriceFinal(),
                petitionDto.getIdCategory(),
                petitionDto.getIdCategory(),
                petitionDto.getNameCategory(),
                petitionDto.getNameCategory(),
                petitionDto.getNumRow(),
                petitionDto.offset()
        }, (rs, arg1) -> new Product(rs.getInt("id"),
                rs.getString("name"),
                rs.getString("url_image"),
                rs.getFloat("price"),
                rs.getInt("discount"),
                rs.getInt("category")));
    }

}
