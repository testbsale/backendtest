package com.api.productos.bsale.productos.service.impl;

import com.api.productos.bsale.productos.model.Category;
import com.api.productos.bsale.productos.model.Dto.PetitionDto;
import com.api.productos.bsale.productos.model.Dto.ResponseDto;
import com.api.productos.bsale.productos.repository.ProductRepository;
import com.api.productos.bsale.productos.service.productService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductImpl implements productService {
    private final ProductRepository productRepository;

    @Autowired
    public ProductImpl(ProductRepository productRepository){
        this.productRepository = productRepository;
    }

    public ResponseDto consultProducts(PetitionDto petitionDto) {
        petitionDto.validatePetition();
        ResponseDto response = new ResponseDto(productRepository.ConsultProducts(petitionDto), petitionDto.getPage(), petitionDto.getNumRow(),0);
        response.calculateTotalPage(productRepository.ConsultProductsCount(petitionDto));
        return response;
    }
    @Override
    public List<Category> consultCategories() {
        return productRepository.consultCategories();
    }
}
