package com.api.productos.bsale.productos.model.Dto;

import com.api.productos.bsale.productos.model.Product;
import lombok.*;

import java.util.List;

@Setter
@Getter
@Builder
@AllArgsConstructor
@ToString
public class ResponseDto {
    private List<Product> products;
    private int page;
    private int numRow;
    private int totalPage;

    public void calculateTotalPage(int totalRow){
        if(getNumRow() == 0){
            setNumRow(5);
        }
        int total = totalRow/getNumRow();
        setTotalPage(total);
    }
}
