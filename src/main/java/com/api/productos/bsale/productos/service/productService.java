package com.api.productos.bsale.productos.service;

import com.api.productos.bsale.productos.model.Category;
import com.api.productos.bsale.productos.model.Dto.PetitionDto;
import com.api.productos.bsale.productos.model.Dto.ResponseDto;

import java.util.List;

public interface productService {
    ResponseDto consultProducts(PetitionDto petitionDto);

    List<Category> consultCategories();
}
