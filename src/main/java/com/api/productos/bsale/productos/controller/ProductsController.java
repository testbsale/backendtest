package com.api.productos.bsale.productos.controller;

import com.api.productos.bsale.productos.model.Category;
import com.api.productos.bsale.productos.model.Dto.GenericDto;
import com.api.productos.bsale.productos.model.Dto.PetitionDto;
import com.api.productos.bsale.productos.model.Product;
import com.api.productos.bsale.productos.service.productService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/Products")
@CrossOrigin(origins = "*", allowedHeaders = "*")
@Api(value = "Products", tags = {"Products"})
public class ProductsController {
    private final productService productService;

    @Autowired
    public ProductsController(productService productService){
        this.productService = productService;
    }


    @GetMapping("/categories")
    @ApiOperation(value = "Returns all categories", response = Category.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successfully retrieved categories")
    })
    public ResponseEntity<GenericDto> consultCategories() {
        return ResponseEntity.ok().body(GenericDto.success(this.productService.consultCategories()));
    }

    @PostMapping("/products")
    @ApiOperation(value = "Returns all products", response = Product.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successfully retrieved products")
    })
    public ResponseEntity<GenericDto> consultProducts(@RequestBody PetitionDto petitionDto) {
        return ResponseEntity.ok().body(GenericDto.success(this.productService.consultProducts(petitionDto)));
    }
}
